const express = require('express');
const mongoose = require('mongoose');

// task route
const taskRoute = require('./Routes/taskRoute.js');

const app = express();
const port = 3001;


mongoose.connect('mongodb+srv://admin:admin@batch245-espiritu.dgm8gby.mongodb.net/s35-discussion?retryWrites=true&w=majority',
        {
            useNewUrlParser:true,
            useUnifiedTopology: true
        }
    );

    let db = mongoose.connection;

    // erro handling in connecting
    db.on("error", console.error.bind(console, "Connection error"));

    // This will be triggered if the connection is succesfull
    db.once("open", ()=> console.log("we're connected to the cloud!"));



//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/tasks', taskRoute)






app.listen(port, ()=> console.log(`Server is running at port: ${port}`))