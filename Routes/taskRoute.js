const express = require('express');
const router = express.Router();

const taskController = require("../Controllers/taskController.js")







router.get('/get', taskController.getAll)

router.post('/createTask', taskController.createTask)

router.delete('/deleteTasks/:id', taskController.deleteTask)

router.get('/getTask/:id', taskController.getTask)

router.put('/updateTask/:id', taskController.updateTask)


module.exports = router;