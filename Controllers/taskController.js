const Task = require("../Models/task.js")

// Controllers and functions

module.exports.getAll = (request, response)=>{
    Task.find({})
    
    .then(result => {
        return response.send(result)
    })
    .catch(error => {
        return response.send(error)
    })
}



//  create task
module.exports.createTask = (request, response)=>{
    const input = request.body;

    Task.findOne( {name: input.name})
    .then(result => {
        if(result !==null){
            return response.send("The task is already existing!")
        }
        else{
            let newTask = new Task({
                name:input.name
            });

            newTask.save()
            .then(()=> response.send('The task is successfully added!'))
            .catch(error => response.send(error))

        }
    })

    .catch( error => response.send(error))
}


// delete task

module.exports.deleteTask = (request, response)=>{
    let idToBeDeleted = request.params.id;

    Task.findByIdAndRemove(idToBeDeleted)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}


// get specific task
module.exports.getTask = (request, response)=>{

    let idToGet = request.params.id;

    Task.findById(idToGet)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}


// get and update task

module.exports.updateTask = (request, response)=>{
    let idToUpdate = request.params.id;

    Task.findByIdAndUpdate(idToUpdate, {status:'Complete'}, {new:true})
    .then(result => response.send(result))
    .catch(error => response.send(error))
  
    
}